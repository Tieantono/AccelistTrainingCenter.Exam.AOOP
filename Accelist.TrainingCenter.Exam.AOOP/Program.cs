﻿using Accelist.TrainingCenter.Exam.AOOP.Models;
using Accelist.TrainingCenter.Exam.AOOP.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP
{
    public static class Program
    {
        static void Main(string[] args)
        {
            new MenuService().PromptLoginMenu();
        }
    }
}
