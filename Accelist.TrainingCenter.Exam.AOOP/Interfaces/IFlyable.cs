﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Interfaces
{
    /// <summary>
    /// The interface for flyable animal
    /// </summary>
    interface IFlyable
    {
        void Fly();
    }
}
