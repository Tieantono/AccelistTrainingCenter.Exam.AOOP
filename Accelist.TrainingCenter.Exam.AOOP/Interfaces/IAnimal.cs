﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Interfaces
{
    /// <summary>
    /// The interface for Animal class
    /// </summary>
    interface IAnimal
    {
        string Name { get; set; }
        int Age { get; set; }
        
        decimal Weight { get; set; }
        
        string Species { get; set; }
    }
}
