﻿using Accelist.TrainingCenter.Exam.AOOP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Models
{
    /// <summary>
    /// Animal class. Must implements the IAnimal interface on this class
    /// </summary>
    public class Animal: IAnimal
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public decimal Weight { get; set; }
        public string Species { get; set; }
    }
}
