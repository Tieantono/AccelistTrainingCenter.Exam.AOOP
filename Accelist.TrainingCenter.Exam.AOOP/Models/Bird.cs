﻿using Accelist.TrainingCenter.Exam.AOOP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Models
{
    /// <summary>
    /// A derived class from the Animal class
    /// </summary>
    public abstract class Bird : Animal
    {
        public void LayEgg()
        {
            Console.WriteLine("I can lay eggs!");
        }
    }
}
