﻿using Accelist.TrainingCenter.Exam.AOOP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Models
{
    /// <summary>
    /// A derived class from the Bird class. Must implements the IFlyable interface
    /// </summary>
    public class Eagle: Bird, IFlyable
    {
        public void Fly()
        {
            Console.WriteLine("I'm flying!");
        }
    }
}
