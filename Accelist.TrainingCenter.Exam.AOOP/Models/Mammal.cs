﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Models
{
    public class Mammal: Animal
    {
        /// <summary>
        /// A derived class from the Animal class
        /// </summary>
        public void GiveBirth()
        {
            Console.WriteLine("I can give a birth!");
        }
    }
}
