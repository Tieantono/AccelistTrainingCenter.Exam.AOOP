﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Models
{
    /// <summary>
    /// A derived class from the Bird class
    /// </summary>
    public class Penguin: Bird
    {
    }
}
