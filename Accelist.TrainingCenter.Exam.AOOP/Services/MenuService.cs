﻿using Accelist.TrainingCenter.Exam.AOOP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Exam.AOOP.Services
{
    public class MenuService
    {
        /// <summary>
        /// MenuService constructor, to instate the Animals property
        /// </summary>
        public MenuService()
        {
            Animals = new List<Animal>();
        }

        /// <summary>
        /// List of Animal property
        /// </summary>
        public static List<Animal> Animals { get; set; }

        /// <summary>
        /// Show the Login menu
        /// </summary>
        public void PromptLoginMenu()
        {
            int loginCount = 0;
            Console.Write("Username: ");
            var username = Console.ReadLine();
            Console.Write("Password: ");
            var password = Console.ReadLine();

            while (username != "ZooAdmin" || password != "ILoveMyZoo123")
            {
                if (loginCount < 2)
                {
                    Console.WriteLine("Invalid credentials");
                    loginCount++;
                    Console.Write("Username: ");
                    username = Console.ReadLine();
                    Console.Write("Password: ");
                    password = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine(@"You have attempted to login with the wrong credentials 3 times. 
We must force you to leave now. 
Press any key to quit the application....");
                    return;
                }
            }
            PromptMainMenu();
        }

        /// <summary>
        /// Show the Main menu
        /// </summary>
        public void PromptMainMenu()
        {
            Console.Clear();
            Console.Write(@"1. Animal List
2. Exit
Please input either 1 or 2: 
");
            var inputNumber = 0;
            while (!Int32.TryParse(Console.ReadLine(), out inputNumber) || (inputNumber < 1 || inputNumber > 2))
            {
                Console.WriteLine("Please input a valid option....");
            }
            if (inputNumber == 1) PromptAnimalListMenu();
            if (inputNumber == 2)
            {
                Console.WriteLine("Goodbye!");
                Console.Read();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Show the Animal List menu
        /// </summary>
        public void PromptAnimalListMenu()
        {
            Console.Clear();
            Console.Write(@"1. Animal Status
2. Add Animal
3. Back
Please input either 1, 2, or 3:
");
            var inputNumber = 0;
            while (!Int32.TryParse(Console.ReadLine(), out inputNumber) || (inputNumber < 1 || inputNumber > 3))
            {
                Console.Write("Please input a valid option....");
            }

            if (inputNumber == 1) PromptAnimalStatusMenu();
            if (inputNumber == 2) PromptAddAnimalMenu();
            if (inputNumber == 3) PromptMainMenu();
            Console.Read();
        }

        /// <summary>
        /// Show the Animal Status menu
        /// </summary>
        public void PromptAnimalStatusMenu()
        {
            if (Animals.Count == 0)
            {
                Console.WriteLine("No animal yet....");
                Console.Read();
            }
            else
            {
                var i = 1;
                foreach (var animal in Animals)
                {
                    Console.WriteLine($"{ i }. { animal.Name }, { animal.Species }");
                    i++;
                }
                Console.Write("Please select the animal using the number(s) listed above: ");
                var inputNumber = 0;
                while (!Int32.TryParse(Console.ReadLine(), out inputNumber) || (inputNumber < 1 || inputNumber > Animals.Count))
                {
                    Console.WriteLine("Please input a valid option....");
                }
                PromptIndividualAnimalStatusMenu(Animals[inputNumber - 1]);
                Console.Read();
                PromptAnimalListMenu();
            }
        }

        /// <summary>
        /// Show the Individual Animal Status menu
        /// </summary>
        /// <param name="animal"></param>
        public void PromptIndividualAnimalStatusMenu(Animal animal)
        {
            Console.WriteLine($"I'm a { animal.Species }");
            Console.WriteLine($"My name is { animal.Name }");
            Console.WriteLine($"My age is { animal.Age }");
            Console.WriteLine($"My weight is { animal.Weight }");
            Console.Write("Press any key go back to the previous menu....");
            Console.Read();
        }

        /// <summary>
        /// Show the Add Animal menu
        /// </summary>
        public void PromptAddAnimalMenu()
        {
            Console.WriteLine(@"Please add a new animal based on these following species
1. Lion
2. Gorilla
3. Eagle
4. Penguin
Please input either 1, 2, 3, or 4:
");
            var inputNumber = 0;
            while (!Int32.TryParse(Console.ReadLine(), out inputNumber) || (inputNumber < 1 || inputNumber > 4))
            {
                Console.WriteLine("Please input a valid option....");
            }

            if (inputNumber == 1) PromptRegisterAnimal("Lion");
            if (inputNumber == 2) PromptRegisterAnimal("Gorilla");
            if (inputNumber == 3) PromptRegisterAnimal("Eagle");
            if (inputNumber == 4) PromptRegisterAnimal("Penguin");
        }

        /// <summary>
        /// Show the Register Animal menu
        /// </summary>
        /// <param name="speciesName"></param>
        public void PromptRegisterAnimal(string speciesName)
        {
            Console.WriteLine("Your animal name: ");
            var name = Console.ReadLine();
            var age = 0;
            Console.WriteLine("Your animal age: ");
            while (!Int32.TryParse(Console.ReadLine(), out age))
            {
                Console.WriteLine("Please input age with integer value....");
            }
            var weight = 0M;
            Console.WriteLine("Your animal weight: ");
            while (!Decimal.TryParse(Console.ReadLine(), out weight))
            {
                Console.WriteLine("Please input weight with decimal value....");
            }

            if (speciesName == "Lion") AddLion(name, age, weight, speciesName);
            if (speciesName == "Gorilla") AddLion(name, age, weight, speciesName);
            if (speciesName == "Eagle") AddLion(name, age, weight, speciesName);
            if (speciesName == "Penguin") AddLion(name, age, weight, speciesName);

            Console.WriteLine($"The { speciesName } was successfully added!");
            PromptAnimalListMenu();
        }

        /// <summary>
        /// This method will add the Lion species into the Animals property
        /// </summary>
        /// <param name="name"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        /// <param name="speciesName"></param>
        public void AddLion(string name, int age, decimal weight, string speciesName)
        {
            Animals.Add(new Lion
            {
                Name = name,
                Age = age,
                Weight = weight,
                Species = speciesName
            });
        }

        /// <summary>
        /// This method will add the Eagle species into the Animals property
        /// </summary>
        /// <param name="name"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        /// <param name="speciesName"></param>
        public void AddEagle(string name, int age, decimal weight, string speciesName)
        {
            Animals.Add(new Eagle
            {
                Name = name,
                Age = age,
                Weight = weight,
                Species = speciesName
            });
        }

        /// <summary>
        /// This method will add the Penguin species into the Animals property
        /// </summary>
        /// <param name="name"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        /// <param name="speciesName"></param>
        public void AddPenguin(string name, int age, decimal weight, string speciesName)
        {
            Animals.Add(new Eagle
            {
                Name = name,
                Age = age,
                Weight = weight,
                Species = speciesName
            });
        }

        /// <summary>
        /// This method will add the Gorilla species into the Animals property
        /// </summary>
        /// <param name="name"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        /// <param name="speciesName"></param>
        public void AddGorilla(string name, int age, decimal weight, string speciesName)
        {
            Animals.Add(new Eagle
            {
                Name = name,
                Age = age,
                Weight = weight,
                Species = speciesName
            });
        }
    }
}
